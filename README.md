# TA_PWChange
Splunk TA to reset the passwords on remote forwarders via deployment server. This app supports both static and automatically-generated passwords.



Original app developed by Skip Cruise.

https://splunkbase.splunk.com/app/2722/
